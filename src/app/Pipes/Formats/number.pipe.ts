import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'number'
})
export class NumberPipe implements PipeTransform {

  transform(value: string): string {
    return (parseFloat(value)).toLocaleString('en-us', { minimumFractionDigits: 0 });
  }

}
