import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditByStatusComponent } from './credit-by-status.component';

describe('CreditByStatusComponent', () => {
  let component: CreditByStatusComponent;
  let fixture: ComponentFixture<CreditByStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditByStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditByStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
