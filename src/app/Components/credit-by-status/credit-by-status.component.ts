import { Component, OnInit } from '@angular/core';
import { CreditService } from 'src/app/Services/CreditService';
import { Credito } from 'src/app/Modals/Credito';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-credit-by-status',
  templateUrl: './credit-by-status.component.html',
  styleUrls: ['./credit-by-status.component.css']
})
export class CreditByStatusComponent implements OnInit {
  creditos: Array<Credito> = new Array<Credito>();
  estado: string

  constructor(private activatedRoute: ActivatedRoute, private oCreditService: CreditService) {
    
    
   }
  ngOnInit() {
    this.activatedRoute.params.subscribe(routeParams => {
      this.estado =routeParams.estado
      this.cargarCreditos()
    });
    
  }
 /**
  * Carga todos los creditos y filtra por estado
  */
  async cargarCreditos() {
    this.creditos = await this.oCreditService.getCredits();
    this.creditos = this.creditos.filter(x=> x.estado==this.estado)
  }

}
