import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditByUserComponent } from './credit-by-user.component';

describe('CreditByUserComponent', () => {
  let component: CreditByUserComponent;
  let fixture: ComponentFixture<CreditByUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditByUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditByUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
