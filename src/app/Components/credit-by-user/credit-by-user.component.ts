import { Component, OnInit } from '@angular/core';
import { Credito } from 'src/app/Modals/Credito';
import { ActivatedRoute } from '@angular/router';
import { CreditService } from 'src/app/Services/CreditService';

@Component({
  selector: 'app-credit-by-user',
  templateUrl: './credit-by-user.component.html',
  styleUrls: ['./credit-by-user.component.css']
})
export class CreditByUserComponent implements OnInit {
  creditos: Array<Credito> = new Array<Credito>();
  documento: string

  constructor(private activatedRoute: ActivatedRoute, private oCreditService: CreditService) {
    
    
   }
  ngOnInit() {
    this.activatedRoute.params.subscribe(routeParams => {
      this.documento =routeParams.documento
      this.cargarCreditos()
    });
    
  }

  /**
   * Carga los creditos que tenga una persona
   */
  async cargarCreditos() {
    this.creditos = await this.oCreditService.getCredits();
    this.creditos = this.creditos.filter(x=> x.cedula==this.documento)
    console.log(this.creditos)
  }

}
