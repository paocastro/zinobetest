import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Credito } from 'src/app/Modals/Credito';
import { CreditService } from 'src/app/Services/CreditService';
import { MessageComponent } from '../message/message.component';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { SaldoxPagarService } from 'src/app/Services/saldox-pagar.service';

@Component({
  selector: 'app-forms-new-credit',
  templateUrl: './forms-new-credit.component.html',
  styleUrls: ['./forms-new-credit.component.css'],
})
export class FormsNewCreditComponent implements OnInit {
  form:FormGroup
  constructor(private router:Router, private formBuilder:FormBuilder,private oCreditService:CreditService,
    public dialog: MatDialog,private saldo:SaldoxPagarService) { 
    this.form= this.formBuilder.group({
      nombre:['',Validators.required],
      email:['',[Validators.required, Validators.email]],
      cedula:['',Validators.required],
      valorSolicitado:[10000,[Validators.required,Validators.min]],
      fechaPago:['']

    })
  }

  ngOnInit() {
  }

  /**
   * Actualiza el valor en formControl cuando se cambia en el control monto  
   * @param value Valor actualizado
   */
  changeValue(value){
    this.form.controls["valorSolicitado"].setValue(value)
  }


  /**
   * Acción ejecutada cuando se confirman que todos los datos requeridos estan,
   * llama al servicio cliente para realizar el insert en bd
   */
  async onSubmit(){
    var credito:Credito=new Credito()

    credito.nombre= this.form.controls["nombre"].value
    credito.correo= this.form.controls["email"].value
    credito.cedula= this.form.controls["cedula"].value
    credito.valorSolicitado= this.form.controls["valorSolicitado"].value
    credito.fechaPago= this.form.controls["fechaPago"].value
    await this.oCreditService.addCredit(credito).then(x=> {
      this.ConfirmacionCierre(x)
    })
    
    
  }

  /**
   * Se encargar de notificar si el credito fue aprobado o negado
   * @param credit 
   */
  ConfirmacionCierre(credit:Credito){

    var titulo:string;
    var mensaje:string;

    if(credit.estado=="Aprobado"){
      titulo="Felicitaciones!!"
      mensaje="Tu crédito fue aprobado"
    }else{
      titulo="Lo sentimos!"
      if(credit.observacion)mensaje= credit.observacion
      else mensaje="Tu crédito fue negado, lamentamos informarte que no te podemos prestar"
    }
    const dialogRef = this.dialog.open(MessageComponent, {
      data: {titulo: titulo, mensaje: mensaje}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(credit.estado=="Aprobado"){
        this.saldo.cambiaSaldo(credit.valorSolicitado)
        this.router.navigate(['/Header/CreditByUser/' + credit.cedula])

      }
      else this.resetFormat()
    });
  }
  /**
   * Reinicia los valores del form
   */
   resetFormat(){
    this.form.controls["nombre"].setValue('')
     this.form.controls["email"].setValue('')
     this.form.controls["cedula"].setValue('')
     this.form.controls["valorSolicitado"].setValue(10000)
    this.form.controls["fechaPago"].setValue('')
   }

}
