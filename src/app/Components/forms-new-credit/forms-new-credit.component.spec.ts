import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsNewCreditComponent } from './forms-new-credit.component';

describe('FormsNewCreditComponent', () => {
  let component: FormsNewCreditComponent;
  let fixture: ComponentFixture<FormsNewCreditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormsNewCreditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsNewCreditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
