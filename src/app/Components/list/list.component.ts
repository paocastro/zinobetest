import { Component, OnInit, Input } from '@angular/core';
import { Credito } from 'src/app/Modals/Credito';
import { MessageComponent } from '../message/message.component';
import { MatDialog } from '@angular/material/dialog';
import { CreditService } from 'src/app/Services/CreditService';
import { SaldoxPagarService } from 'src/app/Services/saldox-pagar.service';

@Component({
  selector: 'list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  @Input() lista:Array<Credito>;
  constructor(public dialog: MatDialog,private oCreditService:CreditService,private saldo:SaldoxPagarService) {
    
   }

  ngOnInit() {
    
  }

  /**
   * Realiza el pago del credito
   * @param item credito a pagar
   */
  pagar(item){
    const dialogRef = this.dialog.open(MessageComponent, {
      data: {titulo: "Pagar", mensaje: "¿Vas a pagar el crédito seleccionado?",confirmacion:true}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.saldo.cambiaSaldo(item.valorSolicitado * -1)
        item.pagoCredito="Si"
        this.oCreditService.pagoCredito(item)
      }
    }) 
  }
}
