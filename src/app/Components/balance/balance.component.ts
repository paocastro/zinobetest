import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { CreditService } from 'src/app/Services/CreditService';
import { Credito } from 'src/app/Modals/Credito';
import { SaldoxPagarService } from 'src/app/Services/saldox-pagar.service';

@Component({
  selector: 'balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.css']
})
export class BalanceComponent implements OnInit {
  saldoBanco:number=0;
  saldoPendientePagar:number=0;

  constructor(private oCreditService: CreditService,private saldo:SaldoxPagarService) { }

  ngOnInit() {
    this.saldoBanco = environment.MaxBank
    this.cargarCreditos()
    this.retomarCambioSaldo()
  }

  /**
   * Valida si el saldo a tenido algun cambio por creacion creditos o pago
   */
  retomarCambioSaldo(){
    this.saldo.item.subscribe(x=>{
      console.log(x)
      this.saldoPendientePagar=this.saldoPendientePagar + parseFloat(x.toString() ) 
    })
  }
  /**
   * Calcula el saldo por pagar de todos los creditos
   */
  async cargarCreditos(){
    var creditos:Array<Credito>= new Array<Credito>();
    creditos= await this.oCreditService.getCredits();
    this.saldoPendientePagar= creditos.filter(x=>x.estado=="Aprobado" && x.pagoCredito=="No").reduce((a, b) => a + (b['valorSolicitado'] || 0), 0);
  }

}
