import { Component, Input, Output, EventEmitter, ChangeDetectorRef} from '@angular/core';

@Component({
  selector: 'monto',
  templateUrl: './monto.component.html',
  styleUrls: ['./monto.component.css']
})
export class MontoComponent {
  value:number=10000;
  @Output() changeValue:EventEmitter<number>= new EventEmitter<number>();

  constructor() { }

  /**
   * Devuelve el valor con prefijo y envia valor actualizado al padre
   * @param value 
   */
  formatLabel=(value: number) =>{
    this.changeValue.emit(value)
    if (value >= 1000) {
      return Math.round(value / 1000) + 'k';
    }
    
    return value;
  }

}
