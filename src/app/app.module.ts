import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './Components/header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatMenuModule} from '@angular/material/menu';
import { MatListModule } from '@angular/material/list';
import { NumberPipe } from './Pipes/Formats/number.pipe';
import { BalanceComponent } from './Components/balance/balance.component';
import { MontoComponent } from './Components/monto/monto.component';
import {MatSliderModule} from '@angular/material/slider';
import { FormsNewCreditComponent } from './Components/forms-new-credit/forms-new-credit.component';
import {MatCardModule} from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MessageComponent } from './Components/message/message.component';
import {MatDialogModule} from '@angular/material/dialog';
import { CreditByUserComponent } from './Components/credit-by-user/credit-by-user.component';
import { CreditByStatusComponent } from './Components/credit-by-status/credit-by-status.component';
import { ListComponent } from './Components/list/list.component';
import {MatDividerModule} from '@angular/material/divider';




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NumberPipe,
    BalanceComponent,
    MontoComponent,
    FormsNewCreditComponent,
    MessageComponent,
    CreditByUserComponent,
    CreditByStatusComponent,
    ListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatSidenavModule,
    MatMenuModule,
    MatListModule,
    MatSliderModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    HttpClientModule,
    MatDialogModule,
    MatDividerModule
  ],
  exports: [
    NumberPipe,
    ],
    entryComponents: [MessageComponent],
  providers: [MatDatepickerModule,HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
