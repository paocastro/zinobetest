import { Credito } from '../Modals/Credito';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConnectApiService } from './ConnectApi.service';

enum estadoType {
    Aprobado = 'Aprobado',
    Rechazado = 'Rechazado'
}
enum pagoType {
    Si = 'Si',
    No = 'No'
}

@Injectable({
    providedIn: 'root'
})
export class CreditService {
    eEstadoType = estadoType;
    constructor(private oConnect: ConnectApiService) { }

    /**
     * llama al servicio que se conecta con el api para realizar el insert
     * valida aleatoriamente si el crédito es aprobado o rechazado
     * @param credit 
     */
    async addCredit(credit: Credito) {
        var creditos;
        creditos = await this.getCreditsByUser(credit.cedula);
        
        var respuestaCreditosNegados = await this.ValidacionCreditosAnteriores(creditos, "Rechazado");
        var repsuestaAdicion
        if (respuestaCreditosNegados) {
            credit = await this.validacionCredito(credit,creditos)
            credit.pagoCredito = pagoType.No
            if(creditos.length ==0 || (creditos.length !=0 && credit.estado==estadoType.Aprobado ))
            {repsuestaAdicion = await this.oConnect.post('add', credit)
            return repsuestaAdicion}
            else return credit
        } else {
            credit.estado = estadoType.Rechazado
            credit.observacion = "Se rechaza por tener créditos anteriores rechazados"
            return credit
        }
    }

    /**
     * Trae los créditos que tenga un usuario almacenado en la base de datos
     * @param documento número de documento de la persona
     */
    async getCreditsByUser(documento: string) {
        var creditos;
        creditos = await (await this.oConnect.get("FindCreditByUser/" + documento)).toPromise()
        return creditos
    }

    /**
     * Trae todos los créditos almacenados
     */
    async getCredits() {
        var creditos;
        creditos = await (await this.oConnect.get("")).toPromise()
        return creditos
    }

    /**
     * Valida si la persona tiene algun crédito en el estado remitido
     * @param creditos Array de creditos de la persona
     * @param estado estado a consulta
     * @returns true: no tiene ningun crédito en ese estado
     *          false: si tiene algun crédito en ese estado
     */
    async ValidacionCreditosAnteriores(creditos: Array<Credito>, estado: string) {
        
        if (creditos.filter(x => x.estado == estado).length == 0) return true
        else return false
    }

    


    /**
     * De manera randomica aprueba los créditos
     * @param credit 
     */
    async validacionCredito(credit: Credito,creditosUser:Array<Credito>) {
        var CreditosPendientePago =true
        if(creditosUser.filter(x => x.pagoCredito == "No").length == 0)CreditosPendientePago=false
       
        if (creditosUser.length==0) {
            let random = Math.floor(Math.random() * 100) + 1
            let modulo = random % 2
            console.log(modulo)
            if (modulo == 0) credit.estado = this.eEstadoType.Aprobado
            else credit.estado = this.eEstadoType.Rechazado
            
        }//Si tiene creditos aprobados pero pagos siempre debe ser aprobado 
        else if(!CreditosPendientePago) {
            credit.estado = this.eEstadoType.Aprobado
        }
        //cuando tenga creditos pendiete no debe permitir sacar un nuevo credito
        else {
            credit.estado = this.eEstadoType.Rechazado
            credit.observacion="Tienes créditos pendientes por pagar"
        }

        return credit
    }

    async pagoCredito(credit: Credito) {
        var body;
        var repsuestaAdicion
        body = { filter: credit._id, update: { $set: { pagoCredito: credit.pagoCredito } } }
        repsuestaAdicion = await this.oConnect.post('upd', body)
        console.log(body)
    }
}