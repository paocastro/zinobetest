import { TestBed } from '@angular/core/testing';

import { SaldoxPagarService } from './saldox-pagar.service';

describe('SaldoxPagarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SaldoxPagarService = TestBed.get(SaldoxPagarService);
    expect(service).toBeTruthy();
  });
});
