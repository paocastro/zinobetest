import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SaldoxPagarService {
  item= new Subject();
  constructor() { }
  
  /**
   * recibe el valor que cambio el y lo emite a todos los suscriptores
   * @param value 
   */
  cambiaSaldo(value){
    this.item.next(value)
  }
}
