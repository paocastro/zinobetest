import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConnectApiService {

  private apiUrl = `${environment.apiUrl}`;


    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    constructor(
        private http: HttpClient
    ) {
    }

    async get(url) {
       return  await  this.http.get(this.apiUrl + "/" +  url)
    }

    async post(url,object){
           var respuesta= await this.http.post(this.apiUrl + "/" + url, object)
            .toPromise()
            return respuesta
    }
}
