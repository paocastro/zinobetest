import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './Components/header/header.component';
import { FormsNewCreditComponent } from './Components/forms-new-credit/forms-new-credit.component';
import { CreditByUserComponent } from './Components/credit-by-user/credit-by-user.component';
import { CreditByStatusComponent } from './Components/credit-by-status/credit-by-status.component';


const routes: Routes = [
  {path:'' , redirectTo:'Header/NewCredit' , pathMatch:'full'},
  { path: 'Header', component: HeaderComponent,
    children:[
      { path: 'NewCredit', component: FormsNewCreditComponent},
      { path: 'CreditByUser/:documento', component: CreditByUserComponent},
      { path: 'CreditByStatus/:estado', component: CreditByStatusComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
