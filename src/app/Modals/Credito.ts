export class Credito {
    _id:string;
    nombre: string;
    cedula: string;
    correo: string;
    valorSolicitado: number;
    fechaPago: Date;
    estado: estadoType;
    pagoCredito: pagoType;
    observacion:string;
}

enum estadoType {
    Aprobado = 'Aprobado',
    Rechazado = 'Rechazado'
}

enum pagoType {
    Si = 'Si',
    No = 'No'
}